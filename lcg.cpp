#include "lcg.h"

LCG::LCG() : seed(1), current_x(1), a(141),
                                    b(28411),
                                    c(134456) {};


LCG::LCG(uint_least32_t seed) : seed(seed), current_x(seed), a(141),
                                                             b(28411),
                                                             c(134456) {};



void LCG::set_seed(uint_least32_t new_seed) {
    current_x = seed = new_seed;
}

LCG::result_type LCG::operator()() {
    return current_x = (current_x * a + b) % c;
}

void LCG::discard() {
    current_x = seed = (uint_least32_t)std::random_device()();
}
