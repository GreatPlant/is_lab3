#ifndef BASEGENERATOR_H
#define BASEGENERATOR_H
#include <cstdint>
#include <random>

class IGenerator
{
public:
    typedef uint32_t result_type;

    virtual ~IGenerator() {};

    virtual result_type min() const = 0;

    virtual result_type max() const = 0;

    virtual result_type operator()() = 0;

    virtual void discard() = 0;

};

#endif // BASEGENERATOR_H
