#ifndef LCG_H
#define LCG_H
#include "basegenerator.h"

class LCG : public IGenerator
{
public:

    LCG();

    LCG(uint_least32_t seed);

    result_type min() const override { return 0; };

    result_type max() const override { return 134455; };

    void set_seed(uint_least32_t new_seed);

    void discard() override;

    result_type operator()() override;

private:
    int_least32_t seed;

    result_type current_x;

    result_type a, b, c;
};

#endif // LCG_H
