#include <string>
#include <cstdint>
#include <iostream>
#include <fstream>
#include <chrono>
#include <algorithm>
#include <iterator>
#include <thread>
#include <sys/types.h>
#include <sys/stat.h>
#include "sha1.h"
#include "lcg.h"

using namespace std::chrono;
using std::string;
using std::cout;
using std::endl;

static std::atomic_bool esc_pressed = false;

size_t get_file_length(const string& file) {
    struct stat filestatus;
    stat(file.c_str(), &filestatus);
    return filestatus.st_size;
}

bool stream_good(const std::fstream& is) {
    return is.is_open() && is.good();
}

std::vector<char> get_buffer(const string& filename) {
    std::fstream is(filename.c_str(), std::ios::in |
                                      std::ios::binary);
    std::vector<char> buffer;
    if(stream_good(is)) {
        size_t filesize = get_file_length(filename);
        buffer.resize(filesize);
        is.read(reinterpret_cast<char *>(buffer.data()), filesize);
        is.close();
    } else {
        cout << "error opening file or his state is bad\n";
    }
    return buffer;
}

void encrypt(const string& filename, unsigned int key) {
    std::vector<char> buffer = get_buffer(filename);
    LCG lcg(key);
    std::uniform_int_distribution<char> uid(0, std::numeric_limits<char>::max());

    std::fstream ostream(filename.c_str(), std::ios::out |
                                           std::ios::binary);

    std::transform(buffer.begin(), buffer.end(),
                   std::ostream_iterator<char>(ostream),
                   [&uid, &lcg](char x) { return x ^ uid(lcg); });

}
int seed_from_hash(string hash) {
    return stoi(hash.substr(36, 44), nullptr, 16);
}

void interactive_loop() {
    SHA1 hashgen;
    string password, filename;
    while(true) {

        cout << "Enter file: ";
        std::cin >> filename;

        cout << "Enter password: ";

        std::cin >> password;

        hashgen.update(password);
        string hash = hashgen.final();
        encrypt(filename, seed_from_hash(hash));
        cout << "File encrypted\n";

    }
}

void wait_esc_key() {
    while (std::cin.get() != 27) {}
    esc_pressed = true;
}
int main(int argc, char** argv)
{
    string file;
    string password;

    switch (argc) {
    case 1: {
        cout << "========Interactive mode========\n";
//        std::thread esc_handler(wait_esc_key);
        interactive_loop();
//        esc_handler.join();
        break;
    }
    case 2:
        cout << "Wrong usage!\n";
        break;
    case 3:
        file = argv[1];
        password = argv[2];
        SHA1 hashgen;
        hashgen.update(password);
        string hash = hashgen.final();
        unsigned seed = stoi(hash.substr(36, 44), nullptr, 16);
        encrypt(file, seed);
        cout << "What's all!\n";
        break;
    }
    return 0;
}
